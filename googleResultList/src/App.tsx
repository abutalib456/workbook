import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

interface SearchBoxProps {
  borderColor: string;
}

interface GoogleLogoProps {
  srcFile: string;
}

interface ButtonProps {
  btnText: string;
}

interface SearchResultProps {
  title: string,
  para: string,
}

function SearchButton ({btnText}: ButtonProps) {
  return <button>{btnText}</button>
}

function GoogleLogo({srcFile}: GoogleLogoProps) {
  return (
    <div className="logo-img">
      <img id='google-logo' src={srcFile} alt="Google"></img>
    </div>
  )
}


function SearchBox () {
  return (
    <div className="search-box">
      <input type='search' className='search-box'></input>
    </div>
  )
}

const SearchResult = ({ header, paragraph }) => {
  return (
    <div className="search-result">
      <h4><a href='{header}'>{header}</a></h4>
      <p>{paragraph}</p>
    </div>
  );
};

const SearchResults =  () => {
  return (
    <div className='search-results'>
      {
        res.map((result, index) => (
          <SearchResult key={index} header={result.title} paragraph={result.para} />
        ))
      }
    </div>
  )
}

const firstResult = {
  title: 'React',
  para: 'React is the library for web and native user interfaces' + 
  ' Build user interfaces out of individual pieces called components written in JavaScript...'
}

const secondResult = {
  title: 'Quick Start',
  para: 'Installation - React Reference Overview - Describing the UI - ...',
}

const thirdResult = {
  title: 'Tutorial: Tic-Tac-Toe',
  para: 'Click on the file labeled styles.css in the Files section of ...',
}

const fourthResult = {
  title: 'Installation',
  para: 'Installation · Start a new React project. If you want to build an ...',
}

const fifthResult = {
  title: 'Start a New React Project',
  para: 'Start a New React Project. If you want to build a new app or a ...',
}

const res = [
  firstResult,
  secondResult,
  thirdResult,
  fourthResult,
  fifthResult,
]
// function AllResults ({SearchResult, SearchResultIndex}: SearchResult) {
//   return (
//     <li className="#" key={SearchResultIndex}>{SearchResult}</li>
//   )
// }

function App() {
  return (
    <div className='res'>
      <GoogleLogo srcFile='../public/googlogo.png' />
      <SearchBox />
      <div className='buttons'>
      <SearchButton btnText='Google Search'/>
      <SearchButton btnText='Im Feeling Lucky' />
      </div>
      <SearchResults />s
      
    </div>
  )
}

export default App
