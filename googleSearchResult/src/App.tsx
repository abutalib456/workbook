import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

interface SearchBoxProps {
  borderColor: string;
}

interface GoogleLogoProps {
  srcFile: string;
}

interface ButtonProps {
  btnText: string;
}

interface SearchResultProps {
  title: string,
  para: string,
}

interface ResultsListProps {
  results: SearchResultProps[];
}

function SearchButton ({btnText}: ButtonProps) {
  return <button onClick={() => 
    (
      console.log("Search")
    )}>
      {btnText}</button>
}

function GoogleLogo({srcFile}: GoogleLogoProps) {
  return (
    <div className="logo-img">
      <img id='google-logo' src={srcFile} alt="Google"></img>
    </div>
  )
}



// function SearchBox (query) {
//   const [query,setQuery] = useState('');
//   return (
//       <input type='search' 
//       className='search-box-input'
//       value={query}
//       onChange={(e) => setQuery(e.target.value)}
//         placeholder="Search...">

//       </input>
//   )
// }

// const SearchResult = ({ header, paragraph }) => {
//   return (
//     <div className="search-result">
//       <h4><a href='{header}'>{header}</a></h4>
//       <p>{paragraph}</p>
//     </div>
//   );
// };


function ResultsList({results}: ResultsListProps) {
  return (
    <ul className='result-list'>
      {results.map((result, index) => (
        <li key={index}><h4><a href={result.title}>{result.title}</a></h4> <br />{result.para}</li>
      ))}
    </ul>
  );
}
// const SearchResults =  () => {
//   return (
//     <div className='search-results'>
//       {
//         res.map((result, index) => (
//           <SearchResult key={index} header={result.title} paragraph={result.para} />
//         ))
//       }
//     </div>
//   )
// }

const firstResult = {
  title: 'React',
  para: 'React is the library for web and native user interfaces' + 
  ' Build user interfaces out of individual pieces called components written in JavaScript...'
}

const secondResult = {
  title: 'Quick Start',
  para: 'Installation - React Reference Overview - Describing the UI - ...',
}

const thirdResult = {
  title: 'Tutorial: Tic-Tac-Toe',
  para: 'Click on the file labeled styles.css in the Files section of ...',
}

const fourthResult = {
  title: 'Installation',
  para: 'Installation · Start a new React project. If you want to build an ...',
}

const fifthResult = {
  title: 'Start a New React Project',
  para: 'Start a New React Project. If you want to build a new app or a ...',
}

const res = [
  firstResult,
  secondResult,
  thirdResult,
  fourthResult,
  fifthResult,
]


const reactData: SearchResultProps[] = [
  { title: 'React Hooks', para: 'Learn about React Hooks.' },
  { title: 'React Router', para: 'Routing in React made easy.' },
  { title: 'React Context', para: 'Manage state with React Context.' },
];

const typescriptData: SearchResultProps[] = [
  { title: 'TypeScript Basics', para: 'Introduction to TypeScript.' },
  { title: 'TypeScript Generics', para: 'Understanding Generics in TypeScript.' },
  { title: 'TypeScript Interfaces', para: 'How to use interfaces in TypeScript.' },
];

// function AllResults ({SearchResult, SearchResultIndex}: SearchResult) {
//   return (
//     <li className="#" key={SearchResultIndex}>{SearchResult}</li>
//   )
// }

function App() {
  const [query, setQuery] = useState('');
  const [results, setResults] = useState<SearchResultProps[]>([]);

  const handleSearch = () => {
    if (query.toLowerCase() === 'react') {
      setResults(reactData);
    } else if (query.toLowerCase() === 'typescript') {
      setResults(typescriptData);
    } else {
      setResults([{ title: 'No results found', para: '' }]);
    }
    console.log(query);
  };
  return (
    <div className='res'>
      <GoogleLogo srcFile='../public/googlogo.png' />
      <div>
        <input
          type="search"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          placeholder="Search..."
        ></input>
      </div>
      <div className='buttons'>
      {/* <SearchButton btnText='Google Search' /> */}

      <button onClick={handleSearch}>Google Search</button>
      
      </div>
      
      
      <ResultsList results={results} />
      
  </div>
  )
}

export default App

