import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

interface SearchBoxProps {
  borderColor: string;
}

interface GoogleLogoProps {
  srcFile: string;
}

interface ButtonProps {
  btnText: string;
}

function SearchButton ({btnText}: ButtonProps) {
  return <button>{btnText}</button>
}

function GoogleLogo({srcFile}: GoogleLogoProps) {
  return (
    <div className="logo-img">
      <img id='google-logo' src={srcFile} alt="Google"></img>
    </div>
  )
}


function SearchBox () {
  return (
    <div className="search-box">
      <input type='search' className='search-box'></input>
    </div>
  )
}

function App() {
  return (
    <>
      <GoogleLogo srcFile='../public/googlogo.png' />
      <SearchBox />
      <div className='buttons'>
      <SearchButton btnText='Google Search'/>
      <SearchButton btnText='Im Feeling Lucky' />
      </div>
    </>
  )
}

export default App
